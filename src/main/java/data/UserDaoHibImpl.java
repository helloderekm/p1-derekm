package data;

import models.Post;
import models.User;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import util.HibernateUtil;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;
import java.util.List;

public class UserDaoHibImpl implements UserDao{

    private Logger logger = LoggerFactory.getLogger(UserDaoHibImpl.class);

    @Override
    public List<String> getAllUsernames() {

        try(Session s = HibernateUtil.getSession()){
            List<String> usernames = s.createQuery("Select username from User").list();
            return usernames;
        }

    }

    @Override
    public List<String> getAllEmails() {
        try(Session s = HibernateUtil.getSession()){
            List<String> emails = s.createQuery("Select email from User").list();
            return emails;
        }
    }

    @Override
    public List<User> getAllUsers() {

        try(Session session = HibernateUtil.getSession()){
            List<User> users = session.createQuery("FROM User", User.class).list();
            return users;
        }
    }

    @Override
    public User getUserById(int userId) {
        try(Session session = HibernateUtil.getSession()){
            User user = session.get(User.class, userId);
            return user;
        }
    }

/*    @Override
    public void addNewUser(User user) {

    }*/

    @Override
    public void deleteUser(int userId) {
        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            session.delete(new User(userId));
            transaction.commit();
        }

    }

    @Override
    public void updateUser(int userId, User user) {
        try(Session session = HibernateUtil.getSession()){
            Transaction transaction = session.beginTransaction();
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaUpdate<User> criteria = builder.createCriteriaUpdate(User.class);
            Root<User> root = criteria.from(User.class);
            criteria.set(root.get("username"), user.getUsername());
            criteria.set(root.get("email"), user.getEmail());
            criteria.where(builder.equal(root.get("userId"), userId));
            session.createQuery(criteria).executeUpdate();
            transaction.commit();
        }
    }

    @Override
    public List<Post> getUserPostsByUserId(int userId) {
        try(Session session = HibernateUtil.getSession()){
            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<Post> cq = builder.createQuery(Post.class);
            Root<Post> root = cq.from(Post.class);
            cq.select(root).where(builder.equal(root.get("user"), userId));

            Query<Post> query = session.createQuery(cq);
            List<Post> results = query.getResultList();

            return results;
        }
    }

    @Override
    public boolean addNewUser(String username, String password, String email, boolean isAdmin) {

        try(Session session = HibernateUtil.getSession()){

            Transaction transaction = session.beginTransaction();
            User user = new User(username, password, email, isAdmin);
            session.save(user);
            transaction.commit();
            return true;
        }

    }

    @Override
    public boolean findUserInDbByUsername(String username) {

        try(Session session = HibernateUtil.getSession()){

            List<String> usernames = session.createQuery("Select username from User").list();

            return usernames.contains(username);
        }

    }

    @Override
    public String getUserPassword(String username, String password) {

        try(Session session = HibernateUtil.getSession()){

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<String> cq = builder.createQuery(String.class);
            Root<User> root = cq.from(User.class);
            cq.select(root.get("password"));
            cq.where(builder.equal(root.get("username"),username));

            return session.createQuery(cq).uniqueResult();


        }

    }

    @Override
    public User getUserByUsername(String username) {

        try(Session session = HibernateUtil.getSession()){

            CriteriaBuilder builder = session.getCriteriaBuilder();
            CriteriaQuery<User> cq = builder.createQuery(User.class);
            Root<User> root = cq.from(User.class);
            cq.select(root).where(builder.equal(root.get("username"), username));

            Query<User> query = session.createQuery(cq);
            query.setMaxResults(1);
            List<User> result = query.getResultList();

            return result.get(0);
        }
    }
}

