package data;

import models.Comment;
import models.Post;
import models.User;

import java.util.List;

public interface PostDao {

    public List<Post> getAllPosts();
    public Post getPostById(int postId);
    public void addNewPost(Post post);
    public void deletePost(int postId);
    public void updatePost(Post post);


    void addNewComment(Comment comment);

    List<Comment> getCommentsByPostId(int postId);
}
