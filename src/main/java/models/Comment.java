package models;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

@Entity
@Table(name = "comments")
public class Comment implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int commentId;
    @ManyToOne
    private Post post;
    @ManyToOne
    private User user;
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date dateCreated;
    private String commentBody;

    public Comment(){
        super();
    }

    public Comment(int commentId, Post post, User user, Date dateCreated, String commentBody){
        this.commentId = commentId;
        this.post = post;
        this.user = user;
        this.dateCreated = dateCreated;
        this.commentBody = commentBody;
    }

    public Comment(int commentId){
        this.commentId = commentId;
    }

    public Comment(int postId, String commentBody) {
    }

    public Comment(Post post, User user, String commentBody) {
        this.post = post;
        this.user = user;
        this.commentBody = commentBody;
    }


    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Date dateCreated) {
        this.dateCreated = dateCreated;
    }

    public String getCommentBody() {
        return commentBody;
    }

    public void setCommentBody(String commentBody) {
        this.commentBody = commentBody;
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

}
