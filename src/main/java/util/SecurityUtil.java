package util;

import io.javalin.http.Context;

public class SecurityUtil {
    public void attachResponseHeaders(Context context){
        context.header("Access-Control-Expose-Headers","Authorization, User-Id");
    }
}
