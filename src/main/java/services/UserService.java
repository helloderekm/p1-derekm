package services;

import data.UserDao;
import data.UserDaoHibImpl;
import io.javalin.http.UnauthorizedResponse;
import models.Post;
import models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;



import java.util.List;

public class UserService {

    private final UserDao userDao = new UserDaoHibImpl();
    private final Logger logger = LoggerFactory.getLogger(UserService.class);

    public List<User> getAll(){
        return userDao.getAllUsers();
    }
    public User getById(int userId){

        User user = userDao.getUserById(userId);
        user.setAdmin(true);

        if(user == null){
            logger.error("you must log in to create a post.");
            throw new UnauthorizedResponse();
        } else{
            return user;
        }

    }


    public void delete(int userId){
        userDao.deleteUser(userId);
    }

    public void update(int userId, User user) {
        userDao.updateUser(userId,user);
    }

    public List<Post> getPosts(int userId) {
        return userDao.getUserPostsByUserId(userId);
    }

    public int registerNewUser(String username, String password, String email, boolean isAdmin) {

        List<String> dbUsernames = userDao.getAllUsernames();
        List<String> dbEmails = userDao.getAllEmails();

        if(dbUsernames.contains(username)){
            logger.warn(username + " already exists in db");
            return 2;
        }

        if(dbEmails.contains(email)){
            logger.warn(email + " already exists in db");
            return 3;
        }

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String newPassword = passwordEncoder.encode(password);

        if(userDao.addNewUser(username,newPassword,email,isAdmin))
            return 1;

        return 0;
    }

    /*    public void add(User user){
        userDao.addNewUser(user);
    }*/

    public User getUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }
}
