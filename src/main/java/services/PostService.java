package services;

import data.PostDao;
import data.PostDaoHibImpl;
import models.Comment;
import models.Post;
import models.User;

import java.util.List;

public class PostService {

    private PostDao postDao = new PostDaoHibImpl();

    public List<Post> getAll(){
        return postDao.getAllPosts();
    }

    public Post getById(int postId){
        return postDao.getPostById(postId);
    }

    public void add(Post post){
        postDao.addNewPost(post);
    }

    public void update(Post post){
        postDao.updatePost(post);
    }

    public void delete(int postId){
        postDao.deletePost(postId);
    }

    public void addComment(Comment comment) {
        postDao.addNewComment(comment);
    }

    public List<Comment> getComments(int postId) {
        return postDao.getCommentsByPostId(postId);
    }
}
