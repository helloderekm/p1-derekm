package services;

import data.UserDao;
import data.UserDaoHibImpl;
import io.javalin.http.Context;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

public class AuthService {

    private final UserDao userDao = new UserDaoHibImpl();
    private final Logger logger = LoggerFactory.getLogger(AuthService.class);

    public boolean checkUsername(String username) {
        return userDao.findUserInDbByUsername(username);
    }

    public boolean checkPassword(String username, String password) {

        PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String dbPassword = userDao.getUserPassword(username, password);

        if(dbPassword == null){
            logger.warn("no password found");
        } else if(passwordEncoder.matches(password,dbPassword)){
            logger.info("password matches");
            return true;
        }

        return false;

    }

    public boolean hasAdminAccess(Context context) {

        String authHeader = context.header("Authorization");
        if(authHeader != null && authHeader.equals("admin-auth-token")){
            return true;
        }

        return false;
    }

    public boolean hasUserAccess(Context context) {

        String authHeader = context.header("Authorization");
        if(authHeader != null && authHeader.equals("user-auth-token")){
            return true;
        }
        return false;
    }
}
